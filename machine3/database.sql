use data;

CREATE TABLE users
(
    id INT PRIMARY KEY NOT NULL,
    username VARCHAR(100),
    password VARCHAR(100)
);

INSERT INTO users VALUES (1,"admin","motdepasse");
INSERT INTO users VALUES (2,"jacques","banana123");
INSERT INTO users VALUES (3,"victor","padok4ever");